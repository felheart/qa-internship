*** Settings ***
Documentation  The resource's file, that opens the browser and shares it for the full screen size. Imported once in _resource.robot
Resource  _resource.robot
Test Setup  Prepare Test Environment
Test Teardown  Close All Browsers

*** Variables ***
${login}     felheart768@gmail.com
${password}     zargatest1
${empty}

*** Test Cases ***
Wrong Username And Password Are Entered
    [Documentation]  Test that users cannot login with wrong username and password
    [Tags]   Wrong_Credentials
    Enter Element Wait Page    css=.right .fa-user    Напомнить пароль
    Enter Username   felheart768@gmail.com1
    Enter Password   zargatest12
    Sign In
    Check That Login And Email Fails

Empty Username Is Entered
    [Documentation]     Test that users cannot login with empty username
    [Tags]   Empty_Username
    Enter Element Wait Page    css=.right .fa-user    Напомнить пароль
    Enter Username  ${empty}
    Enter Password  ${password}
    Sign In
    Check That Login Fails

Empty Password Is Entered
    [Documentation]     Test that users cannot login with empty password
    [Tags]  Empty_Password
    Enter Element Wait Page    css=.right .fa-user    Напомнить пароль
    Enter Username    ${login}
    Enter Password    ${empty}
    Sign In
    Check That Login Fails

Valid Username And Password Are Entered
    [Documentation]  Data is filled the fields automaticaly and the personal cabinet is opened after. Confirmation the fact you logged in into your personal cabinet
    [Tags]  Smoke
    Enter Element Wait Page    css=.right .fa-user    Напомнить пароль
    Input Text    login-login    ${login}
    Input password  login-pass    ${password}
    Click And Element Visability    css=span.button    css=.email
    Enter Element Wait Page    css=.email     Личный кабинет пользователя: felheart768@gmail.com