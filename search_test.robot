*** Settings ***
Documentation  The resource's file, that opens the browser and shares it for the full screen size. Imported once in _resource.robot
Resource  _resource.robot
Test Setup  Prepare Test Environment
Test Teardown  Close All Browsers

*** Variables ***
${login}     felheart768@gmail.com
${password}     zargatest1

*** Test Cases ***
Login to site
    [Documentation]  Data is filled the fields automaticaly. Data is entered in the search and verification is performed for confirmation.
    [Tags]  Smoke
    Enter Element Wait Page    css=.right .fa-user    Напомнить пароль
    Input Text    login-login    ${login}
    Input password  login-pass    ${password}
    Click And Element Visability    css=span.button    css=.email
    Input Text   header-search-input   Брюки
    Enter link and expect two elements    css=span.fa-search    css=.goods-padding:nth-child(1) .link    Брюки Z25991    css=.goods-padding:nth-child(2) .link     Брюки Z25982
    Input Text   header-search-input   Пальто
    Enter link and expect two elements    css=span.fa-search    css=.goods-padding:nth-child(1) .link    Пальто Z24658    css=.goods-padding:nth-child(2) .link     Пальто Z24646
    Input Text   header-search-input   Боди
    Enter link and expect two elements    css=span.fa-search    css=.goods-padding:nth-child(1) .link    Боди Z24458    css=.goods-padding:nth-child(2) .link     Боди Z24454