*** Settings ***
Documentation   The resource's file, that opens the browser and shares it for the full screen size. Imported once in _resource.robot
Resource    _resource.robot
Test Setup  Prepare Test Environment
Test Teardown   Close All Browsers

*** Variables ***


*** Test Cases ***
Cart test
    [Documentation]    The test puts the products into the basket and checks the righting of adding.
    [Tags]  Smoke
    Input Text   header-search-input   КОСТЮМ Z25113
    Click Show Title    css=span.fa-search    Поиск. Магазин женской одежды ZARGA.
    Click Show Title    css=a.goods-img    Костюм Z25113. Магазин женской одежды ZARGA.
    Wait Until Page Contains Element    css=.padding-box .num1 img
    Wait Until Page Contains Element    xpath=//*[@id="goods-main"]/h2
    Wait Until Page Contains Element    xpath=//*[@id="goods-main"]/div[2]/span[1]/span[2]
    Wait Until Page Contains Element    css=.ua-price
    ${price} =   get text   //*[@id="goods-main"]/div[2]/span[1]/span[2]
    Should Be Equal     ${price}    450 грн
    Select From List    css=#size-select   42
    Click And Element Visability    css=span.buy-button    xpath=//*[@id="tobasket-popup"]/a
    Click Show Title    css=#tobasket-popup a    Корзина. Магазин женской одежды ZARGA.
    ${price_cart} =   get text   css=#basket-border-box .retail2 span
    Should Be Equal     ${price_cart}    450
    ${amount_cart} =   get value   css=.qty-input
    Should Be Equal     ${amount_cart}    1
    ${total_price} =   get text   css=#bt-retail2
    Should Be Equal     ${total_price}    ${price_cart}
    Click Element  css=#header-main .logo
    Input Text   header-search-input   ЛОСИНЫ Z23114
    Click Show Title    css=span.fa-search    Поиск. Магазин женской одежды ZARGA.
    Click Show Title    css=a.goods-img    Лосины Z23114. Магазин женской одежды ZARGA.
    Click Element   css=span.fa-plus
    Select From List    css=#size-select   42
    Click And Element Visability    css=span.buy-button    xpath=//*[@id="tobasket-popup"]/a
    Click Show Title    css=#tobasket-popup a    Корзина. Магазин женской одежды ZARGA.
    ${second_price_cart} =  get text   css=#basket-border-box .basket-item:nth-child(1) .retail span
    Should Be Equal     ${second_price_cart}    1174
    ${second_amount_cart} =  get value   css=#basket-border-box .basket-item:nth-child(1) .qty-box .qty-input
    Should Be Equal     ${second_amount_cart}    2
    ${second_total_price} =  get text   css=#bt-retail
    Should Be Equal     ${second_total_price}    1624
    Click Element   css=#basket-border-box .basket-item:nth-child(1) .qty-box .minus
    ${second_amount_cart} =  get value   css=#basket-border-box .basket-item:nth-child(1) .qty-box .qty-input
    Should Be Equal     ${second_amount_cart}    1
    ${second_price_cart} =  get text   css=#basket-border-box .basket-item:nth-child(1) .retail2 span
    Should Be Equal     ${second_price_cart}    587
    ${second_total_price} =  get text   css=#bt-retail2
    Should Be Equal     ${second_total_price}    1037
    Click And Element Visability    css=a#basket-clear-link span.no-mobi420     css=.popup-title
    Click Element   css=#message-basket-popup span.red
    Input Text   header-search-input   ПЛАТЬЕ Z25302
    Click Show Title    css=span.fa-search    Поиск. Магазин женской одежды ZARGA.
    Click Show Title    css=a.goods-img     Платье Z25302. Магазин женской одежды ZARGA.
    Wait Until Page Contains Element    css=.ua-price
    ${thitd_price} =  get text   //*[@id="goods-main"]/div[2]/span[1]/span[2]
    Should Be Equal     ${thitd_price}    500 грн
    Select From List    css=#size-select   42
    Click And Element Visability    css=span.buy-button     xpath=//*[@id="tobasket-popup"]/a
    Click Show Title    css=#tobasket-popup a    Корзина. Магазин женской одежды ZARGA.
    ${thitd_price_cart} =   get text   css=#basket-border-box .retail2 span
    Should Be Equal     ${thitd_price_cart}    500
    ${thitd_amount_cart} =   get value   css=.qty-input
    Should Be Equal     ${thitd_amount_cart}    1
    ${thitd_total_price} =   get text   css=#bt-retail2
    Should Be Equal     ${thitd_total_price}    ${thitd_price_cart}
    Click Element   css=#basket-button-box .retail2
    Input Text    last_name   Адамков
    Input Text    first_name   Максим
    Input Text    patronymic   Сергеевич
    Input Text    phone   0681107139
    Input Text    email   felheart768@gmail.com
    Select From List    css=#delivery-select-ua   Укрпочта
    Select From List    css=#paid-select-ua   Наложенный платеж
    Input Text    city   Хмельницкий
    Input Text    zip_code   29000
    Input Text    address   Институтская 20
    Input Text    desc   Тест


