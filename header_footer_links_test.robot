*** Settings ***
Documentation  The resource's file, that opens the browser and shares it for the full screen size. Imported once in _resource.robot
Resource  _resource.robot
Test Setup  Prepare Test Environment
Test Teardown  Close All Browsers

*** Variables ***


*** Test Cases ***
Cheking the links in the side's header
    [Documentation]  Check the link in the header of site and make sure that we have moved to the right page
    [Tags]  Smoke+
    Enter link   link=Доставка    xpath=//*[@id="static-page-border"]/h1    Способы доставки
    Enter link   link=Оплата    xpath=//*[@id="static-page-border"]/h1    Способы оплаты
    Enter link   link=Возврат    xpath=//*[@id="static-page-border"]/h1    Возврат
    Enter link   link=Оптовые условия    xpath=//*[@id="static-page-border"]/h1    Оптовые условия
    Enter link   link=Контакты    xpath=//*[@id="static-page-border"]/h1    Контакты
    Click Show Title    css=.fav    Список желаний. Магазин женской одежды ZARGA.
    Click Show Title    css=.basket-link    Корзина. Магазин женской одежды ZARGA.
    click element  css=#header-main .logo
    Wait Until Page Contains Element  css=.cat-link-padding
    Enter Element    css=#header-main .logo    css=.cat-link-padding

Cheking the links in the side's footer
    [Documentation]  Check the link in the footer of site and make sure that we have moved to the right page
    [Tags]  Smoke
    Enter link    link=Размерные сетки    xpath=//*[@id="static-page-border"]/h1    Размерные сетки
    Enter link    link=Файлы для скачивания    xpath=//*[@id="message-page"]/div/div/span[1]    404
    Click Show Title    css=div.third span.text-box:nth-child(1) a    Новинки. Магазин женской одежды ZARGA.
    Click Show Title    css=div.third span.text-box:nth-child(2) a    ТОП-100. Магазин женской одежды ZARGA.
    Click Show Title    css=div.third span.text-box:nth-child(3) a    Распродажа. Магазин женской одежды ZARGA.
    Click And Element Visability    css=div.first span.text-box:nth-child(7) span    css=#feedback-popup
