*** Settings ***
Library  Selenium2Library

*** Variables ***
${BROWSER}          Chrome
${HOST}             http://zarga.od.ua/

*** Keywords ***
Prepare Test Environment
    Open Browser   ${HOST}   ${BROWSER}
    Maximize Browser Window
    Wait Until Page Contains Element  css=.cat-link-padding

Enter Element Wait Page  [Arguments]   ${element_name}   ${wait_page}
    Click Element   ${element_name}
    Wait Until Page Contains   ${wait_page}

Click And Element Visability  [Arguments]   ${element_name}   ${expected_element}
    Click Element   ${element_name}
    Wait Until Element Is Visible   ${expected_element}    10sec

Enter link  [Arguments]   ${link_name}   ${expected_element}   ${expected_text}
    Click link   ${link_name}
    Page Should Contain Element   ${expected_element}   ${expected_text}

Click Show Title    [Arguments]  ${element_name}   ${expected_title}
    click element  ${element_name}
    Title Should Be  ${expected_title}

Enter link and expect two elements  [Arguments]   ${element_name}  ${expected_element}   ${expected_text}   ${expected_element2}   ${expected_text2}
    Click element   ${element_name}
    Page Should Contain Element   ${expected_element}   ${expected_text}
    Page Should Contain Element   ${expected_element2}   ${expected_text2}

Enter Element  [Arguments]   ${element_name}   ${wait_element}
    Click Element   ${element_name}
    Wait Until Page Contains Element   ${wait_element}


Enter Username   [Arguments]     ${username}
    Wait Until Page Contains Element    login-login
    Input Text  login-login   ${username}

Enter Password   [Arguments]     ${password}
    Wait Until Page Contains Element    login-pass
    Input Password  login-pass     ${password}

Sign In
    Click Element    css=span.button

Check That Login Fails
    Page Should Contain Element   css=.error    10sec

Check That Login And Email Fails
    Page Should Contain     Неверный логин или пароль