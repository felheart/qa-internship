<h1>This project was created for the QA's internship.</h1>
We will cover with the tests site http://zarga.od.ua in this project.

<h2>In the _resource.robot file:</h2>
<ul>
<li>Occurs the browser opening;</li>
<li>Occurs connection to the site http://zarga.od.ua.</li>
</ul>

<h2>In the file login_test.robot:</h2>
<ul>
<li>The browser is opened and the site is loaded;</li>
<li>The data is filled automatically, and the personal account is opened after;</li>
<li>Confirmation that you are logged in to your personal account.</li>
</ul>

<h2>In the file header_footer_links_test.robot:</h2>
<ul>
<li>Implemented two test cases;</li>
<li>In the first test case, all links in the site are checked and occurs the confirmation on the each page;</li>
<li>In the second test case, all links in the footer of the site are checked and occurs the confirmation on the each page.</li>
</ul>
<h2>In the file cart_test.robot:</h2>
<ul>
<li>Occurs the browser opening;</li>
<li>Occurs connection to the site http://zarga.od.ua;</li>
<li>The browser is opening in the full screen size;</li>
<li>Automatically writing the product's name into the search field;</li>
<li>Appears the search result;</li>
<li>Opening the product in the full screen size;</li>
<li>Checking the next characteristics: image, title, tutorial, price;</li>
<li>Using the price as a variable;</li>
<li>Choosing the product's size;</li>
<li>Adding the chosen product into the basket;</li>
<li>Opening the basket;</li>
<li>Assure that we open the basket;</li>
<li>Checking the price;</li>
<li>Using the price as a variable;</li>
<li>Checking the amount of the products in the basket;</li>
<li>Checking the total sum;</li>
<li>Assure that the chosen product price and the total price are identical;</li>
<li>Go to the main page;</li>
<li>Automatically writing the second product's name into the search field;</li>
<li>Appears the search result;</li>
<li>Opening the product in the full screen size;</li>
<li>Using the price as a variable;</li>
<li>Choosing two these products;</li>
<li>Choosing the product's size;</li>
<li>Adding the chosen product into the basket;</li>
<li>Opening the basket;</li>
<li>Assure that we open the basket;</li>
<li>Checking the second chosen two product's sum;</li>
<li>Using the sum as a variable;</li>
<li>Checking the amount of the products in the basket;</li>
<li>Checking the total sum;</li>
<li>Deleting one of the two simple products in the basket;</li>
<li>Assuring that we've got one product from previous two simple;</li>
<li>Checking the second price;</li>
<li>Checking the total sum;</li>
<li>Cleaning the basket;</li>
<li>Automatically writing the product's name into the search field;</li>
<li>Appears the search result;</li>
<li>Opening the product in the full screen size;</li>
<li>Using the price as a variable;</li>
<li>Choosing the product's size;</li>
<li>Adding the chosen product into the basket;</li>
<li>Opening the basket;</li>
<li>Assure that we open the basket;</li>
<li>Checking the price;</li>
<li>Using the price as a variable;</li>
<li>Checking the amount of the products in the basket;</li>
<li>Checking the total price;</li>
<li>Assure that the chosen product price and the total price are identical;</li>
<li>Confirming the order;</li>
<li>Entering the personal information: Surname, name, middle name, phone, email, delivery method, payments, city, index, address, description. </li>
</ul>



